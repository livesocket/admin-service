FROM golang:alpine as base
RUN apk --no-cache add git ca-certificates g++
WORKDIR /repos/admin-service
ADD go.mod go.sum ./
RUN go mod download

FROM base as builder
WORKDIR /repos/admin-service
ADD . .
RUN CGO_ENABLED=0 GOOS=linux go build -o admin-service

FROM scratch as release
COPY --from=builder /repos/admin-service/admin-service /admin-service
EXPOSE 8080
ENTRYPOINT ["/admin-service"]

