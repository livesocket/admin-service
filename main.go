package main

import (
	"log"
	"os"
	"os/signal"

	"gitlab.com/livesocket/admin-service/actions"
	"gitlab.com/livesocket/admin-service/migrations"
	"gitlab.com/livesocket/service/v2"
)

func main() {
	s := &service.Service{}
	close := s.Init(service.Actions{
		"private.admin.get":     actions.Get(s),
		"private.admin.isAdmin": actions.IsAdmin(s),
	}, nil, "__admin_service", migrations.CreateAdminTable)
	defer close()

	// Wait for CTRL-c or client close while handling events.
	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt)
	select {
	case <-sigChan:
	case <-s.Done():
		log.Print("Router gone, exiting")
		return
	}
}
